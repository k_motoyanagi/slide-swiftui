# SwiftUI

---

## About me?

開発部バックエンドユニット２所属  
ITエンジニアとしては定年が過ぎています。

---

## 宣言的UIの時代

<img data-src="/img/declarative_frameworks.svg">

Note:
WebフロントエンドではReact、Vueがメインストリーム、
アプリでもFlutter、ReactNativeがそれなりのシェアを獲得する中で、
AndroidではJetPackComposeがつい先日Beta版になるなど、
宣言的UIプログラミングは避けては通れない流れになってきています。

---

## 宣言的UIとは?

---

`$$ UI = f(State) $$`

[Flutter - Start thinking declaratively](https://flutter.dev/docs/development/data-and-backend/state-mgmt/declarative) より

Note:
Flutterのドキュメントの中で宣言的であることがどういうことなのか？というのを説明している式を例に見てみます。
UIは画面です、fは関数です、Stateはアプリの状態変数です。
宣言的UIとは関数と状態からUIを構築するための方法のことを指すと考えてよいでしょう。

---

## UIKit時代の宣言的UI

Note:
理論上、状態と関数からUIを生成出来ればいいのでUIKitの時代でも宣言的にUIを構築するスタイルのプログラミングを行うことは出来ました。

---

### [ReSwift](https://github.com/ReSwift/ReSwift)
SwiftでReduxパターンを実現させるライブラリ

---

こんなやつ  
<img class="img-l" data-src="/img/redux.svg">

---

`$$ UI = View(State) $$`

こんな感じの考え方で実装していけば  
宣言的な実装は出来る。

---

出来るんだけど…  
なかなかスマートにはいきません  
<img class="img-l" data-src="/img/redux_note.svg">

Note:
単純にStateからViewを全て再構築すると計算コストが高くなり過ぎて実用的ではありません。
宣言的UIとして売りに出されているライブラリはUI更新を効率よく行うために差分更新のアルゴリズムを用いているものが一般的ですが、
UIKitでは差分更新を効率的に解決する手段が乏しかった。

---

現実的な解決手段としては  
RX系ライブラリとのあわせ技で解決する  
というのがメインストリームでした。  

<sub><sup>参考：[ReSwift の State を RxSwift っぽく使えるようにする](http://hamasyou.com/blog/2017/05/10/swift-reswift-rxswift/)</sup></sub>

Note:
Stateの変更を監視しつつ、変更のあった項目のみをフィルタしてその項目に対するViewのみを更新する。

---

差分更新機能を有する  
UIライブラリも存在しましたが…

* <sub><sup>[ComponentKit](https://componentkit.org/) - ReactインスパイアのFacebook製UIライブラリ</sup></sub>
* <sub><sup>[VTree](https://github.com/inamiy/VTree) - Elm/virtual-domインスパイアのUIライブラリ</sup></sub>

Note:
I/FがUIKitのものからかけ離れており学習コストが高くついたり、UIKitのViewの機能を網羅できていないなどの理由から実用性が低かった。

---

これまでのiOS開発における宣言的UIの実現には  
__”UIの効率的かつ現実的な更新方法”__  
が不足していたのです。

---

## SwiftUIの登場

NOTE:
2019年のWWDCにて発表され、iOS13以上の端末で利用可能になりました。
このUIフレームワークによって、宣言的UIを実現するための効率的かつ現実的なUI更新が可能となりました。

---

## SwiftUIの特徴

* 宣言的なUIの構築
  * 内部的に差分更新を採用しているため、UIの効率的な更新が可能
* プレビューツール
  * Xocdeに組み込まれたプレビューツールにより、実装がどのようなUIになるのかをリアルタイムに確認出来る
* iPhoneだけでなく、 Mac、AppleWatchなどでも利用可能

---

## SwiftUI開発の風景

<video data-autoplay src="/movie/preview.mov"></video>

Note:
これまでもInterfaceBuilder上でリアルタイムプレビューらしきことは可能でしたが、SwiftUIのプレビューでは実装とプレビューがよりシームレスな形で実現可能になっています。

---

## SwiftUIの基本

よくあるカウンターのサンプルです。

<div class="col">

```swift
struct MyView: View {
    // View内部の状態は
    // StatePropertyWrapperを指定する
    @State private var count = 0

    var body: some View {
        VStack(spacing: 16) {
            Text("\(self.count)")
            CountButton(counte: self.$count)
        }
    }
}
```

</div>

<div class="col">

```swift
struct CountButton: View {
    // BindingPropertyWrapperを指定することで
    // Stateとのデータバインディングを実現
    @Binding var count: Int

    var body: some View {
        HStack(spacing: 40) {
            Button("+") { self.count += 1 }
            Button("-") { self.count -= 1 }
        }
    }
}
```

</div>

Note:
データバインディングを実現しつつ、実装量も少なく機能を実現することが出来る

---

## SwiftUIの実装を理解するために抑えておきたい言語機能

Note:
一見するとどのように動作しているのか理解出来ないよに見えますが、Swiftのいくつかの言語機能を理解するとSwiftUIのコードがどのように解決されているのかを理解しやすくなります。

---

### FunctionBuilder

コンパイルタイムマクロのような機能、  
SwiftUIのViewはこの機能によって定義される`ViewBuilder`が利用されています。

```swift
// ViewBuilderの機能を一部抜粋
@_functionBuilder public struct ViewBuilder {
    // 何も指定しなければEmptyViewを返す
    static func buildBlock() -> EmptyView
    // ifによる分岐はOptional（noneの場合何もないことになるのでEmptyViewになる）
    static func buildIf<Content: View>(_ content: Content?) -> Content?
    // 複数のViewを記述する場合はTupleとして一つのViewとして処理されている
    static func buildBlock<C0: View, C1: View>(_ c0: C0, _ c1: C1) -> TupleView<(C0, C1)>
}

// GroupViewのイニシャライザ、
// コンテンツ生成ブロックにViewBuilderアノテーションが付与されている
extension Group : View where Content : View {
    init(@ViewBuilder content: () -> Content)
}
```

Note:
ViewBuilderというアノテーションをつけることで、対象の実装内容から別の型に変換が行われている。
これによって一つのクロージャーブロックに複数のViewを記述したり、ifによる分岐を共通の型として扱うことなどを可能にしている。
尚、FunctionBuilderの仕様は公式には公開されていないが、機能としては利用することが出来るので独自のアノテーションを作ることも可能です。

---

### PropertyWrapper

プロパティのアクセスを中継するオブジェクトを設定します。  
この機能によって、SwiftUIのViewの不変性とStateの可変性を両立させています。

```swift
// これはSwiftUIのStateを擬似的に表現した実装、
// セットした値は別のオブジェクトによって保持されるためstructからメモリが独立出来ることが確認出来る
class Storage<T> {
    var value: T
    init(_ value: T) { self.value = value }
}
@propertyWrapper
struct MyState<Value> {
    private var storage: Storage<Value>
    var wrappedValue: Value {
        get { self.storage.value }
        set { self.storage.value = newValue }
    }
    inti(wrappedValue: Value) { self.storage = Storage(wrappedValue) }
}
```

Note:
SwiftUIには他にも様々なPropertyWrapperが登場しますが、型定義からどのような実装かある程度推測することが出来るでしょう。

---

## SwiftUIではまだ無理なところ

---

SwiftUIではまだUIKitの機能を全て利用することは出来ず、そういった場合`UIViewRepresentable`、`UIViewControllerRepresentable`というプロトコルを介してUIKitの機能にアクセスする必要があります。

---

UIViewの場合
```swift
struct WebView: UIViewRepresentable {
    func makeUIView(context: Context) -> some UIView {
        WKWebView()
    }

    func updateUIView(_ uiView: UIViewType, context: Context) {
        // 更新処理はここに書く
    }
}
```

UIViewControllerの場合
```swift
struct ImagePicker: UIViewControllerRepresentable {
    typealias Context = UIViewControllerRepresentableContext<ImagePicker>

    func makeUIViewController(context: Context) -> some UIViewController {
        UIImagePickerController()
    }

    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        // 更新処理はここに書く
    }
}
```

Note:
各Representableプロトコルを適合したViewを利用することでUIKitの機能をSwiftUIの更新サイクルに対応させて利用することが可能になります。

---

## SwiftUIで使えると便利な機能

---

### Path

CoreGraphicsと似たようなAPIを利用して、  
部分角丸三角、アニメーションと組み合わせてオリジナルのローディングインジケーターなどが作れます。

[Path](https://developer.apple.com/documentation/swiftui/path)

---

### FetchRequest

PropertyWrapperを利用してSwiftUIとCoreDataを連携させることが出来ます。

[FetchRequest](https://developer.apple.com/documentation/swiftui/fetchrequest)

Note:
ローカルDBにはRealmの採用を考えがちですが、SwiftUIではCoreDataの採用を再検討しても良さそうです。

---

### EnvironmentValues

PropertyWrapperを利用して、Viewの様々な状態にアクセスすることが出来ます。  
色々あって把握するのが大変ですが理解していると何かと便利です。

[EnvironmentValues](https://developer.apple.com/documentation/swiftui/environmentvalues)

---

### 質問など

---

# Thank You!
